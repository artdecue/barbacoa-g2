== Lista de bebidas

// Separadas en con/sin alcohol
// Ordenadas por orden alfabético

=== Con alcohol

* Beefeater
* Bombay
* Cerveza
* Jack daniel's
* Larios
* Larios 12
* Larios Rose
* Whisky
* Vino

=== Sin alcohol

* Agua
* Agua con gas
* Aquarios de limon
* Café
* Cerveza sin
* Coca Cola
* Fanta de fresa
* Fanta de limón
* Fanta de naranja
* Leche entera
* Leche semidesnatada
* Leche sin lactosa
* Nestea
* Radler limon
* Tónica
* Zumo de naranja
* Zumo de piña
* Zumo de uva
